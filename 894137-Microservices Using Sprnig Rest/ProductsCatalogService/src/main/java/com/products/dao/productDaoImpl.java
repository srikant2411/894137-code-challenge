package com.products.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.products.model.Product;

@Component
public class productDaoImpl implements productDao{

	@Autowired
	JdbcTemplate jdbctemplate;
	@Override
	public List<Product> getProduct() {

		return jdbctemplate.query("select * from product", new productRowMapper());
	}

}
