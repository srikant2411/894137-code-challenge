package com.authentication.exceptions;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.authentication.models.ErrorDetails;

@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler{

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> handleResourseNotFoundException(ResourceNotFoundException ex,WebRequest req){
		
		ErrorDetails error = new ErrorDetails(new Date(),HttpStatus.BAD_REQUEST.value(),HttpStatus.BAD_REQUEST.toString(),ex.getMessage(), req.getDescription(false));
		
		return new ResponseEntity<ErrorDetails>(error, HttpStatus.BAD_REQUEST);
		
	}
}
