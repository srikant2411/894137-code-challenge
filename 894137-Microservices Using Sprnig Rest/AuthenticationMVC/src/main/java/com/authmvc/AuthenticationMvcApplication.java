package com.authmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = "com.authmvc")
@ComponentScan(basePackages = {"com.authmvc"})
public class AuthenticationMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationMvcApplication.class, args);
	}

}
