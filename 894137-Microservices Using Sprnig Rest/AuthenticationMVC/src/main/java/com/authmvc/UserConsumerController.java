package com.authmvc;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.authmvc.exception.*;



@Controller
public class UserConsumerController {
	@Autowired
	UserServiceClient service;
	
	@Autowired
	
	ProductServiceClient proservice;
	
	@Autowired
	CartServiceClient cartService;
	
	@RequestMapping(value="/", method= RequestMethod.GET)
	public String getAllUsers(){
		
		return "index";
	}
	@RequestMapping(value="/auth", method= RequestMethod.GET)
	public String getAuthuser(@PathVariable("username") String username,@PathVariable("password") String password) {
		System.out.println(service.authenticateuser(username, password));
		return "index";
	}
	@RequestMapping(value="/userlogin", method = RequestMethod.POST)
	public String login(Model model, @RequestParam String username,@RequestParam String password,HttpSession session) {
		ApplicationUser u=null;
		try {
		u= service.authenticateuser(username, password);
		session.setAttribute("userid", u.getUserId());
		System.out.println(u);
		}
		catch(Exception e) {
			throw new InvalidUser("Invalid username and password");
		}
		System.out.println(proservice.getProductsAll());
		List<Product> productlist = proservice.getProductsAll();
		System.out.println(productlist);
		model.addAttribute("productlist", productlist);
		return "products";
	}
	
	@RequestMapping(value="/insertcart", method =  RequestMethod.GET)
	public String AddItemToCart(Model model,@RequestParam int id,@RequestParam int userId,@RequestParam int productId,@RequestParam String productName,@RequestParam int productPrice,@RequestParam int qunatity,@RequestParam int subTotal) {
		Cart car = new Cart();
		System.out.println(id+" "+userId+" "+productName+" "+qunatity);
		try {
		car.setId(id);
		car.setUserId(userId);
		car.setProductId(productId);
		car.setProductName(productName);
		car.setProductPrice(productPrice);
		car.setQuantity(qunatity);
		car.setSubTotal(subTotal);
		System.out.println(car);
		cartService.addCart(car);
		}
		catch(Exception e) {
			List<Product> productlist = proservice.getProductsAll();
			System.out.println(productlist);
			model.addAttribute("productlist", productlist);
			return "products";
			
		}
		List<Product> productlist = proservice.getProductsAll();
		System.out.println(productlist);
		model.addAttribute("productlist", productlist);
		return "products";
	}
}
