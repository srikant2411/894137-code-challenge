package com.products;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
@EnableEurekaClient
@SpringBootApplication
public class ProductsCatalogServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductsCatalogServiceApplication.class, args);
	}

}
