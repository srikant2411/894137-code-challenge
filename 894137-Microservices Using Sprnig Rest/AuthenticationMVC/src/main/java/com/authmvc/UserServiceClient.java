package com.authmvc;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "user-provider",url = "http://localhost:9091")
public interface UserServiceClient {
	
	@GetMapping("/auth/users")
	public List<ApplicationUser> getUsers();
	
	@GetMapping("/auth/user/{username}/{password}")
	public ApplicationUser authenticateuser(@PathVariable("username") String username,@PathVariable("password") String password);
	
}
