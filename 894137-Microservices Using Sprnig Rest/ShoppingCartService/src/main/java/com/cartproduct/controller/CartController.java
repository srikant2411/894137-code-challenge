package com.cartproduct.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cartproduct.model.Cart;
import com.cartproduct.service.CartService;

@RestController
@RequestMapping("/cart")
public class CartController {
	@Autowired
	CartService service;
	
	@PostMapping("/add")
	public ResponseEntity<Object> addItemsToCart(@RequestBody Cart c){
		
		try {
			service.insertCart(c);
			System.out.println(c);
			
			return new ResponseEntity<>(c,HttpStatus.CREATED);
		}catch(Exception e) {
			return new ResponseEntity<>("Bad request", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/get/{userId}")
	public List<Cart> getallcart(@PathVariable("userId") int userId) {
		
		
		return service.getCartItems(userId);
	}
	
	
	

}
