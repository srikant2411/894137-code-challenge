package com.authentication.dao;

import java.util.List;

import com.authentication.models.ApplicationUser;

public interface UsersDao {
public List<ApplicationUser> findall();
public ApplicationUser authenticate(String username,String password);

}
