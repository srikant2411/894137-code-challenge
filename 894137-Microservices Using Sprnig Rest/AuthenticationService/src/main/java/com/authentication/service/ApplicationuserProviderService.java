package com.authentication.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.authentication.dao.UsersDao;
import com.authentication.exceptions.ResourceNotFoundException;
import com.authentication.models.ApplicationUser;


@Service
public class ApplicationuserProviderService {
	@Autowired
	UsersDao user;
	
	public List<ApplicationUser> gerUsers(){
		return user.findall();
	}
	
	public ApplicationUser AuthenticateUser(String username, String password) {
		ApplicationUser appuser=user.authenticate(username, password) ;
		if(appuser!=null) {
			return appuser;
		}
		else {
			throw new ResourceNotFoundException("Invalid username or password"+username+" "+password);
		}
		
	}
	

}
