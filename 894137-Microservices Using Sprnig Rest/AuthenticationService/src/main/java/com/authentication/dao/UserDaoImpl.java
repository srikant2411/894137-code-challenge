package com.authentication.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.authentication.models.ApplicationUser;

@Component
public class UserDaoImpl implements UsersDao{
	@Autowired
	JdbcTemplate jdbctemplate;

	@Override
	public List<ApplicationUser> findall() {
		
		return jdbctemplate.query("select * from applicationuser", new UsersRowMapper());
	}

	@Override
	public ApplicationUser authenticate(String username, String password) {

		return jdbctemplate.query("select * from applicationuser where username=? and password=?", new UserResultSetExtractor(),username,password);
	}
	

}
