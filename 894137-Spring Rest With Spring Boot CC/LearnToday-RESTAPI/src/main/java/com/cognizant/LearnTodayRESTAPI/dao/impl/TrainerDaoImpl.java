package com.cognizant.LearnTodayRESTAPI.dao.impl;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import com.cognizant.LearnTodayRESTAPI.dao.exception.InvalidTrainerIdException;
import com.cognizant.LearnTodayRESTAPI.model.Trainer;

@Component
public class TrainerDaoImpl implements com.cognizant.LearnTodayRESTAPI.dao.TrainerDao {


	@Autowired
	JdbcTemplate template;
	
	@Override
	public int signUp(Trainer trainer) throws DuplicateKeyException {

		return template.update("insert into trainer values (?,?)", trainer.getTrainerId(), trainer.getPassWord());
	}

	@Override
	public int updatePassWord(int trainerId, Trainer trainer) throws InvalidTrainerIdException {
		String sql = "update trainer set Password=? where TrainerId=?";
		int rowsAffected = template.update(sql, trainer.getPassWord(), trainerId);
		if(rowsAffected==0)
			throw new InvalidTrainerIdException("Searched data not found");
		return rowsAffected;
	}

}
