package com.cognizant.LearnTodayRESTAPI.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import com.cognizant.LearnTodayRESTAPI.dao.CourseDao;
import com.cognizant.LearnTodayRESTAPI.model.Course;
import com.cognizant.LearnTodayRESTAPI.rowmapper.CourseRowMapper;

@Component
public class CourseDaoImpl implements CourseDao{
	


	@Autowired
	JdbcTemplate template;

	@Override
	public List<Course> getAllCourses() {

		return template.query("select * from course", new CourseRowMapper());
	}

	@Override
	public List<Course> getCourseById(int courseId) {
		String sql = "select * from course where courseId=?";
		CourseRowMapper crm = new CourseRowMapper();
		return template.query(sql, crm, courseId);
	}

}
