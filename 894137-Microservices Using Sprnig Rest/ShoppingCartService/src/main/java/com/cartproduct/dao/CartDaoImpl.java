package com.cartproduct.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cartproduct.model.Cart;
@Component
public class CartDaoImpl implements CartDao {
	@Autowired
	JdbcTemplate jdbctemplate;
	
	@Override
	public List<Cart> getCartItems(int userId) {
		
		return jdbctemplate.query("select * from cart where userId=", new CartRowMapper(),userId);
	}

	@Override
	public int addCartItems(Cart cartitem) {

		return jdbctemplate.update("insert into cart values(?,?,?,?,?,?,?)",cartitem.getId(),cartitem.getUserId(),cartitem.getProductId(),cartitem.getProductName(),cartitem.getProductPrice(),cartitem.getQuantity(),cartitem.getSubTotal());
	}

	@Override
	public boolean deleteCartItem(int userId, int productId) {
		// TODO Auto-generated method stub
		return false;
	}

}
