package com.products.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.products.model.Product;

public class productRowMapper implements RowMapper<Product> {

	@Override
	public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
		Product pro =new Product();
		pro.setProductId(rs.getInt(1));
		pro.setProductName(rs.getString(2));
		pro.setProductPrice(rs.getInt(3));
		pro.setProductDesc(rs.getString(4));
		pro.setProductUrl(rs.getString(5));
		return pro;
	}

}
