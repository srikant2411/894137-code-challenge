package com.authentication.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.authentication.models.ApplicationUser;

public class UsersRowMapper implements RowMapper<ApplicationUser>{

	@Override
	public ApplicationUser mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ApplicationUser user = new ApplicationUser();
		user.setUsername(rs.getString(1));
		user.setPassword(rs.getString(2));
		user.setUserId(rs.getInt(3));
		
		
		return user;
	}

}
