package com.authmvc;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "cart-provider", url = "http://localhost:9098")
public interface CartServiceClient {
	
	@GetMapping("/cart/add")
	public int addCart(Cart c);

}
