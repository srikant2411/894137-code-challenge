package com.products.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.products.dao.productDao;
import com.products.model.Product;

@Service
public class ProductService {
	@Autowired
	productDao pro;
	
	public List<Product> getAllProducts(){
		
		return pro.getProduct();
	}

}
