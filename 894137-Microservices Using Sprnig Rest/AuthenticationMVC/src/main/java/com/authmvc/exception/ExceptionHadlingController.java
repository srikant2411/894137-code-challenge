package com.authmvc.exception;

import org.springframework.ui.Model;
import com.authmvc.exception.*;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;



@ControllerAdvice
public class ExceptionHadlingController {
	
	@ExceptionHandler(InvalidUser.class)
	public String duplicateResouse(InvalidUser ex, Model model) {
		String mes = ex.getMessage();
		model.addAttribute("message", mes);
		model.addAttribute("check",1);
	
		return "index";
	}
}
