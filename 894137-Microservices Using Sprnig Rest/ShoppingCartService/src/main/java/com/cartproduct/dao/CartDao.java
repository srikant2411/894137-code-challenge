package com.cartproduct.dao;

import java.util.List;

import com.cartproduct.model.Cart;

public interface CartDao {
public List<Cart> getCartItems(int userId);
public int addCartItems(Cart cartitem);
public boolean deleteCartItem(int userId,int productId);
}
