package com.authmvc;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name="products-provider",url="http://localhost:9099")
public interface ProductServiceClient {

	@GetMapping("/product/all")
	public List<Product> getProductsAll();
	
}
