package com.authentication.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.authentication.models.ApplicationUser;
import com.authentication.service.ApplicationuserProviderService;

@RestController
@RequestMapping("/auth")
public class AuthController {
	
	@Autowired
	private ApplicationuserProviderService service;
	
	@GetMapping("/users")
	public List<ApplicationUser> getAllUsers(){
		return service.gerUsers();
	}
	
	@GetMapping("/user/{username}/{password}")
	public ApplicationUser user(@PathVariable("username") String username,@PathVariable("password") String password) {
		
		return service.AuthenticateUser(username, password);
	}

}
