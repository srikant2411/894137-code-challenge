package com.cartproduct.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cartproduct.model.Cart;

public class CartRowMapper implements RowMapper<Cart> {

	@Override
	public Cart mapRow(ResultSet rs, int rowNum) throws SQLException {
		Cart c =new Cart();
		c.setId(rs.getInt(1));
		c.setUserId(rs.getInt(2));
		c.setProductId(rs.getInt(3));
		c.setProductName(rs.getString(4));
		c.setProductPrice(rs.getInt(5));
		c.setQuantity(rs.getInt(6));
		c.setSubTotal(rs.getInt(7));
		
		return c;
	}

}
