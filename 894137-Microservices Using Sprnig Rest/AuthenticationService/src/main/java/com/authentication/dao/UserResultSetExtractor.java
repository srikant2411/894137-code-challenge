package com.authentication.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.authentication.models.ApplicationUser;

public class UserResultSetExtractor implements ResultSetExtractor<ApplicationUser>{

	@Override
	public ApplicationUser extractData(ResultSet rs) throws SQLException, DataAccessException {
		ApplicationUser user=null;
		if(rs.next()) {
			user= new ApplicationUser();
			user.setUsername(rs.getString(1));
			user.setPassword(rs.getString(2));
			user.setUserId(rs.getInt(3));
			
			
		}
		return user;
	}
	

}
