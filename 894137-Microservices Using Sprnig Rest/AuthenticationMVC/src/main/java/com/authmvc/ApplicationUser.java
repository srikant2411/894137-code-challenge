package com.authmvc;

public class ApplicationUser {
	private String username;
	private String password;
	private int userId;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "ApplicationUser [username=" + username + ", password=" + password + ", userId=" + userId + "]";
	}
	

}
