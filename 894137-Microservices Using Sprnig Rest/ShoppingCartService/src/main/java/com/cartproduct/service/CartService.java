package com.cartproduct.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cartproduct.dao.CartDao;
import com.cartproduct.model.Cart;

@Service
public class CartService {

	@Autowired
	CartDao dao;
	
	public void insertCart(Cart c) {
		dao.addCartItems(c);
	}
	public List<Cart> getCartItems(int userId){
		return dao.getCartItems(userId);
	}
}
